1、ros是melodic版本及之前的版本，在下载后的文件夹中打开终端，运行 sudo sh replace_erzhu2_python2.7.sh 或者 sudo sh replace_赵虚左_python2.7.sh （两个命令都可以，只需要运行其中的一条命令，不要两个都运行，两个命令的区别是资源一个是在赵虚左老师的库中，一个是拷贝到了自己的库中）。运行这两条其中一条的命令后，没有问题即什么都不会出现，就可以继续运行sudo rosdep init + rosdep update

2、ros是noetic版本的，把 sudo sh replace_erzhu2_python2.7.sh 或者 sudo sh replace_赵虚左_python2.7.sh 这两条命令换成 sudo sh replace_erzhu2_python3.sh 或者 sudo sh replace_赵虚左_python3.sh ，步骤同1一样。

3、 **注意：** 在没有用上面的步骤之前，自己就把sudo rosdep init运行成功了，然后rosdep update会出现错误，此时会在/etc/ros/rosdep/sources.list.d/中产生一个名叫20-default.list文件，这就需要先把此文件删除，再做1或者2的步骤。删除此文件需要在此目录下打开终端，运行sudo rm 20-default.list。

4、 **注意：** 下载的文件放在哪里都可以，没有具体要求的路径，解决问题后就可以把文件删除。